// server/index.js

  /*const sqlite3 = require('sqlite3').verbose()
  const util = require('util');

  const db = new sqlite3.Database("./server/coin.db", sqlite3.OPEN_READWRITE, (err) =>{
    if (err) return console.error(err.message)});*/

//db.run('CREATE TABLE ltc(date, coin_value)');

//const sql = 'INSERT INTO btc(date, coin_value) VALUES(?,?)';

/*const fetch = require("node-fetch");

var myHeaders = new fetch.Headers();
myHeaders.append("apikey", "uBl4wpRol6uICvcZL23Jm16DoHoATD7Q");

var requestOptions = {
  method: 'GET',
  redirect: 'follow',
  headers: myHeaders
};

const data = ""

fetch("https://api.apilayer.com/fixer/timeseries?base=BTC&symbols=USD&start_date=2021-01-01&end_date=2021-12-31", requestOptions)
  .then(response => response.text())
  .then((res) => {
    let dataTxt = res;
    dataTxt = dataTxt.substring(1);
    dataTxt = dataTxt.substring(dataTxt.indexOf("{") + 1);
    let dataArr = dataTxt.split(",");
    for (let i = 0; i < dataArr.length; i++)
    {
      dataArr[i] = dataArr[i].split('{');
    }
    
    for (let j = 0; j < dataArr.length; j++)
    {
      for (let k = 0; k < dataArr[j].length; k++)
    
      {
          dataArr[j][k] = dataArr[j][k].replace('"', "");
          dataArr[j][k] = dataArr[j][k].replace(":", "");
          dataArr[j][k] = dataArr[j][k].replace("U", "");
          dataArr[j][k] = dataArr[j][k].replace("D", "");
          dataArr[j][k] = dataArr[j][k].replace("S", "");
          dataArr[j][k] = dataArr[j][k].replace("}", "");
          dataArr[j][k] = dataArr[j][k].replace('"', "");
          dataArr[j][k] = dataArr[j][k].replace(/\r?\n|\r/g, "");
          dataArr[j][k] = dataArr[j][k].replace(/\s/g, '');
      }
    }
    
    for (let s=0; s < dataArr.length; s++)
    {
      const sql = 'INSERT INTO btc(date, coin_value) VALUES(?,?)';
      db.run(sql,dataArr[s], (err) =>{
        if (err) return console.error(err.message);
      });
    }
    console.log("data was added to the table");

    db.close((err) =>{  
      if (err) return console.error(err.message);
    });
  })
  .catch(error => console.log('error', error));*/

  /*const https = require('https');

  var options = {
    "method": "GET",
    "hostname": "rest.coinapi.io",
    "path": "/v1/ohlcv/BITSTAMP_SPOT_LTC_USD/history?period_id=1DAY&time_start=2021-01-01T00:00:00&time_end=2022-01-01T00:00:00&limit=400",
    "headers": {'X-CoinAPI-Key': 'B47DCB16-D480-4752-B257-F996E3CABBC1'}
  };
  
  var request = https.request(options, function (response) {
    var chunks = "";
    var msg = ""
    response.on("data", function (chunk) {
      chunks=chunks+(chunk.toString());
    }).on('end',function(){
      chunks = chunks.substring(chunks.indexOf("{") + 1);
      let chunkArr = chunks.split('{');
      for (let i = 0; i<chunkArr.length;i++)
      {
        chunkArr[i] = chunkArr[i].split(',');
        for (let j = 0 ; j<chunkArr[i].length; j++)
        {
          chunkArr[i][j] = chunkArr[i][j].split(':');
        }
      }
      for (let x = 0;x<chunkArr.length;x++)
      {
        msg = msg + chunkArr[x][0][1].substring(2, 12) + ";" + chunkArr[x][4][1] + ',';
      }
      let dataArr = msg.split(",");
      for(let i = 0;i<dataArr.length;i++)
      {
        dataArr[i] = dataArr[i].split(";");
      }

      for (let s=0; s < dataArr.length; s++)
      {
        const sql = 'INSERT INTO ltc(date, coin_value) VALUES(?,?)';
        db.run(sql,dataArr[s], (err) =>{
          if (err) return console.error(err.message);
        });
      }
      console.log("data was added to the table");
  
      db.close((err) =>{  
        if (err) return console.error(err.message);
      });
    })
  });
  
  request.end();*/

  //NOTE: 
  //Due to the fact that coinapi.io and fixer.io allow only 100 daily querys,
  //I implemented a code that will only be needed to be executed once, then the data will be sent
  //to the database.
  //Above is the code I used to initiate the database.


//btc dataBase

const { response } = require("express");
const express = require("express");
const internal = require("stream");

const PORT = process.env.PORT || 3001;

const app = express();

app.get('/bitcoin', (req, res) => {

  res.set('Access-Control-Allow-Origin', '*');

  const sqlite3 = require('sqlite3').verbose()
  const util = require('util');

  const db = new sqlite3.Database("./server/coin.db", sqlite3.OPEN_READWRITE, (err) =>{
    if (err) return console.error(err.message);

    console.log("connection successfull")
  });

  console.log("entered bitcoin query!");
  
  let msg = "";

let getPromise = util.promisify(db.get.bind(db));
let queries = [];
for (let m = 1; m <= 12; m++) {
  let sql;
  if (m==1 || m==3 || m==5 || m == 7 || m == 8 || m == 10 || m == 12)
  {
    for (let d = 1; d<= 31; d++)
    {
      if(d<10 && m<10)
      {
        sql = 'SELECT * FROM btc WHERE date = "2021-0' + m + "-0" + d + '"';
      }
      else if (m<10)
      {
        sql = 'SELECT * FROM btc WHERE date = "2021-0' + m + "-" + d + '"';
      }
      else if (d<10)
      {
        sql = 'SELECT * FROM btc WHERE date = "2021-' + m + "-0" + d + '"';
      }
      else
      {
        sql = 'SELECT * FROM btc WHERE date = "2021-' + m + "-" + d + '"';
      }
      queries.push(getPromise(sql));
    }
  }
  if(m==4 || m==6 || m==9 || m==11)
  {
    for (let d = 1; d<= 30; d++)
    {
      if(d<10 && m<10)
      {
        sql = 'SELECT * FROM btc WHERE date = "2021-0' + m + "-0" + d + '"';
      }
      else if (m<10)
      {
        sql = 'SELECT * FROM btc WHERE date = "2021-0' + m + "-" + d + '"';
      }
      else if (d<10)
      {
        sql = 'SELECT * FROM btc WHERE date = "2021-' + m + "-0" + d + '"';
      }
      else
      {
        sql = 'SELECT * FROM btc WHERE date = "2021-' + m + "-" + d + '"';
      }
      queries.push(getPromise(sql));
    }
  }
  if (m == 2)
  {
    for (let d = 1; d<= 28; d++)
    {
      if (d<10)
      {
        sql = 'SELECT * FROM btc WHERE date = "2021-0' + m + "-0" + d + '"';
      }
      else
      {
        sql = 'SELECT * FROM btc WHERE date = "2021-0' + m + "-" + d + '"';
      }
      queries.push(getPromise(sql));
    }
  }
}
Promise.all(queries).then(
  function(results) {
    let msg = '';
    for (let i = 0; i<results.length;i++)
    {
      msg = msg + results[i].date + ";" + results[i].coin_value + ',';
    }
    msgArr = msg.split(',');
    for (let i = 0; i<msgArr.length; i++)
    {
      msgArr[i] = msgArr[i].split(';')
      msgArr[i][1] = parseFloat(msgArr[i][1])
    }
    console.log(msgArr);
    res.json({message: msgArr});
  }, function(err) {
    console.error(err.message);
  });
  db.close((err) =>{  
    if (err) return console.error(err.message);
  });
});

//eth dataBase

app.get('/ethereum', (req, res) => {

  res.set('Access-Control-Allow-Origin', '*');

  const sqlite3 = require('sqlite3').verbose()
  const util = require('util');

  const db = new sqlite3.Database("./server/coin.db", sqlite3.OPEN_READWRITE, (err) =>{
    if (err) return console.error(err.message);

    console.log("connection successfull")
  });

  console.log("entered ethereum query!");
  
  let msg = "";

let getPromise = util.promisify(db.get.bind(db));
let queries = [];
for (let m = 1; m <= 12; m++) {
  let sql;
  if (m==1 || m==3 || m==5 || m == 7 || m == 8 || m == 10 || m == 12)
  {
    for (let d = 1; d<= 31; d++)
    {
      if(d<10 && m<10)
      {
        sql = 'SELECT * FROM eth WHERE date = "2021-0' + m + "-0" + d + '"';
      }
      else if (m<10)
      {
        sql = 'SELECT * FROM eth WHERE date = "2021-0' + m + "-" + d + '"';
      }
      else if (d<10)
      {
        sql = 'SELECT * FROM eth WHERE date = "2021-' + m + "-0" + d + '"';
      }
      else
      {
        sql = 'SELECT * FROM eth WHERE date = "2021-' + m + "-" + d + '"';
      }
      queries.push(getPromise(sql));
    }
  }
  if(m==4 || m==6 || m==9 || m==11)
  {
    for (let d = 1; d<= 30; d++)
    {
      if(d<10 && m<10)
      {
        sql = 'SELECT * FROM eth WHERE date = "2021-0' + m + "-0" + d + '"';
      }
      else if (m<10)
      {
        sql = 'SELECT * FROM eth WHERE date = "2021-0' + m + "-" + d + '"';
      }
      else if (d<10)
      {
        sql = 'SELECT * FROM eth WHERE date = "2021-' + m + "-0" + d + '"';
      }
      else
      {
        sql = 'SELECT * FROM eth WHERE date = "2021-' + m + "-" + d + '"';
      }
      queries.push(getPromise(sql));
    }
  }
  if (m == 2)
  {
    for (let d = 1; d<= 28; d++)
    {
      if (d<10)
      {
        sql = 'SELECT * FROM eth WHERE date = "2021-0' + m + "-0" + d + '"';
      }
      else
      {
        sql = 'SELECT * FROM eth WHERE date = "2021-0' + m + "-" + d + '"';
      }
      queries.push(getPromise(sql));
    }
  }
}
Promise.all(queries).then(
  function(results) {
    let msg = '';
    for (let i = 0; i<results.length;i++)
    {
      msg = msg + results[i].date + ";" + results[i].coin_value + ',';
    }
    msgArr = msg.split(',');
    for (let i = 0; i<msgArr.length; i++)
    {
      msgArr[i] = msgArr[i].split(';')
      msgArr[i][1] = parseFloat(msgArr[i][1])
    }
    console.log(msgArr);
    res.json({message: msgArr});
  }, function(err) {
    console.error(err.message);
  });
  db.close((err) =>{  
    if (err) return console.error(err.message);
  });
});

//ltc dataBase

app.get('/litecoin', (req, res) => {

  res.set('Access-Control-Allow-Origin', '*');

  const sqlite3 = require('sqlite3').verbose()
  const util = require('util');

  const db = new sqlite3.Database("./server/coin.db", sqlite3.OPEN_READWRITE, (err) =>{
    if (err) return console.error(err.message);

    console.log("connection successfull")
  });

  console.log("entered litecoin query!");
  
  let msg = "";

let getPromise = util.promisify(db.get.bind(db));
let queries = [];
for (let m = 1; m <= 12; m++) {
  let sql;
  if (m==1 || m==3 || m==5 || m == 7 || m == 8 || m == 10 || m == 12)
  {
    for (let d = 1; d<= 31; d++)
    {
      if(d<10 && m<10)
      {
        sql = 'SELECT * FROM ltc WHERE date = "2021-0' + m + "-0" + d + '"';
      }
      else if (m<10)
      {
        sql = 'SELECT * FROM ltc WHERE date = "2021-0' + m + "-" + d + '"';
      }
      else if (d<10)
      {
        sql = 'SELECT * FROM ltc WHERE date = "2021-' + m + "-0" + d + '"';
      }
      else
      {
        sql = 'SELECT * FROM ltc WHERE date = "2021-' + m + "-" + d + '"';
      }
      queries.push(getPromise(sql));
    }
  }
  if(m==4 || m==6 || m==9 || m==11)
  {
    for (let d = 1; d<= 30; d++)
    {
      if(d<10 && m<10)
      {
        sql = 'SELECT * FROM ltc WHERE date = "2021-0' + m + "-0" + d + '"';
      }
      else if (m<10)
      {
        sql = 'SELECT * FROM ltc WHERE date = "2021-0' + m + "-" + d + '"';
      }
      else if (d<10)
      {
        sql = 'SELECT * FROM ltc WHERE date = "2021-' + m + "-0" + d + '"';
      }
      else
      {
        sql = 'SELECT * FROM ltc WHERE date = "2021-' + m + "-" + d + '"';
      }
      queries.push(getPromise(sql));
    }
  }
  if (m == 2)
  {
    for (let d = 1; d<= 28; d++)
    {
      if (d<10)
      {
        sql = 'SELECT * FROM ltc WHERE date = "2021-0' + m + "-0" + d + '"';
      }
      else
      {
        sql = 'SELECT * FROM ltc WHERE date = "2021-0' + m + "-" + d + '"';
      }
      queries.push(getPromise(sql));
    }
  }
}
Promise.all(queries).then(
  function(results) {
    let msg = '';
    for (let i = 0; i<results.length;i++)
    {
      msg = msg + results[i].date + ";" + results[i].coin_value + ',';
    }
    msgArr = msg.split(',');
    for (let i = 0; i<msgArr.length; i++)
    {
      msgArr[i] = msgArr[i].split(';')
      msgArr[i][1] = parseFloat(msgArr[i][1])
    }
    console.log(msgArr);
    res.json({message: msgArr});
  }, function(err) {
    console.error(err.message);
  });
  db.close((err) =>{  
    if (err) return console.error(err.message);
  });
});

//listen

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
