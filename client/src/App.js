import "./App.css";
import { BrowserRouter as Router, Routes, Route} from "react-router-dom";
import Home from "./Pages/Home";
import ErrorPage from "./Pages/ErrorPage";
import BitCoin from "./Pages/BitCoin";
import Ethereum from "./Pages/Ethereum";
import LiteCoin from "./Pages/LiteCoin";

function App() {
  //const [data, setData] = React.useState([]);

  /*React.useEffect(() => {
    fetch("/api")
      .then((res) => res.json())
      .then((data) => setData(data.message));
  }, []);*/
  
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/bitcoin" element={<BitCoin />} />
        <Route path="/litecoin" element={<LiteCoin />} />        
        <Route path="/ethereum" element={<Ethereum />} />
        <Route path="*" element={<ErrorPage />} />
      </Routes>
    </Router>
  );
}

export default App;
