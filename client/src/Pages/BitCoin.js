import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Line } from "react-chartjs-2";
import {Chart as chartjs, Title, Tooltip, LineElement, Legend, CategoryScale, LinearScale, PointElement, Filler} from "chart.js";
import bitcoinImg from '../images/bitcoin.jpg'

chartjs.register(
  Title, Tooltip, LineElement, Legend, CategoryScale, LinearScale, PointElement, Filler
)

function BitCoin() {
  const [data, setData] = React.useState(null);
  const [dates, setDates] = React.useState(null);
  const [coinValue, setCoinValue] = React.useState(null);
  const [chartData, setChartData] = useState(null);


  React.useEffect(() => {
    fetch("http://localhost:3001/bitcoin")
      .then((res) => res.json())
      .then((data) => {
        const twoDimensionArr = data.message;
        setData(twoDimensionArr);
        setDates(twoDimensionArr.map(item => item[0]));
        setCoinValue(twoDimensionArr.map(item => item[1]));
        setChartData({
          labels: twoDimensionArr.map(item => item[0]),
          datasets: [{
            label: "value of BTC in USD",
            data: twoDimensionArr.map(item => item[1]),
            backgroundColor: 'lightblue',
            borderColor: 'darkblue',
            tension: 0.1,
            fill: true
          }]
        })
      })
  }, []);

  return (
    <div style={{textAlign:"center", fontFamily:"Comic Sans MC", fontSize:"100"}}>

      <nav>
        <Link to="/" style={{padding: "10px"}}> Home </Link>
      </nav>

      <img src={bitcoinImg} style={{textAlign: "left", margin: "10px"}}/>
      {chartData && <Line
      data = {chartData}
      style ={{textAlign: "right", margin: "10px"}}
      width = {600}/>
      }
    </div>
  )
}

export default BitCoin;
