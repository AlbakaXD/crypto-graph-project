import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Line } from "react-chartjs-2";
import {Chart as chartjs, Title, Tooltip, LineElement, Legend, CategoryScale, LinearScale, PointElement} from "chart.js";
import LiteCoinImg from '../images/Litecoin.jpg'

chartjs.register(
  Title, Tooltip, LineElement, Legend, CategoryScale, LinearScale, PointElement
)

function LiteCoin() {
  const [data, setData] = React.useState(null);
  const [dates, setDates] = React.useState(null);
  const [coinValue, setCoinValue] = React.useState(null);
  const [chartData, setChartData] = useState(null);


  React.useEffect(() => {
    fetch("http://localhost:3001/litecoin")
      .then((res) => res.json())
      .then((data) => {
        const twoDimensionArr = data.message;
        setData(twoDimensionArr);
        setDates(twoDimensionArr.map(item => item[0]));
        setCoinValue(twoDimensionArr.map(item => item[1]));
        setChartData({
          labels: twoDimensionArr.map(item => item[0]),
          datasets: [{
            label: "value of LTC in USD",
            data: twoDimensionArr.map(item => item[1]),
            backgroundColor: 'rgb(235,130,130)',
            borderColor: 'rgb(200,30,30)',
            tension: 0.1,
            fill: true
          }]
        })
      })
  }, []);

  return (
    <div style={{textAlign:"center", fontFamily:"Comic Sans MC", fontSize:"100", padding: "19px"}}>

      <nav>
        <Link to="/" style={{padding: "10px"}}> Home </Link>
      </nav>

      <img src={LiteCoinImg} style={{textAlign: "left", margin: "10px"}}/>
      {chartData && <Line
      data = {chartData}
      style ={{textAlign: "right", margin: "10px"}}
      width = {600}/>
      }
    </div>
  )
}

export default LiteCoin;
