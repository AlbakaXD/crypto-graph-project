import React from "react";
import { Link } from "react-router-dom";

function ErrorPage() {
  return (<div style={{textAlign:"center", fontFamily:"Comic Sans MC", fontSize:"100"}}>
    ERROR! PAGE NOT FOUND
    
    <nav>
        <Link to="/" style={{padding: "10px"}}> Home </Link>
    </nav>
    </div>);
}

export default ErrorPage;
