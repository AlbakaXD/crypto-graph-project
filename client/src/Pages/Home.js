import "./Home.css";
import React from "react";
import { Link } from "react-router-dom";
import BitCoinImg from "../images/bitcoin.jpg"
import ethImg from "../images/eth.jpg"
import litecoinImg from "../images/Litecoin.jpg"
import Logo from "../images/logo.jpg"


function Home() {
  return(
    <div style={{textAlign:"center", fontFamily:"Comic Sans MC", fontSize:"100", margin: "10px"}}>
      <img src= {Logo}/>

      <nav style={{padding: "80px"}}>
      <Link to="/bitcoin" style={{margin: "100px"}}>
        <img src= { BitCoinImg }/>
      </Link>

      <Link to="/litecoin" style={{margin: "10px"}}>
        <img src= { litecoinImg }/>
      </Link>

      <Link to="/ethereum" style={{margin: "100px"}}>
        <img src= { ethImg }/>
      </Link>
      </nav>
    </div>
  )
}

export default Home;
